#!/bin/sh

# $1 - name of Heroku app
# $2 - Heroku API key
if [ -z "$1" ] || [ -z "$2" ]
then
  echo "Provide proper parameters in your GitLab."
  exit 1
else
  # shellcheck disable=SC1083
  ID=$(docker inspect registry.heroku.com/"$1"/web --format={{.Id}}) # image id
  BODY='{"updates":[{"type":"web","docker_image":"'"$ID"'"}]}'
  curl -n -X PATCH https://api.heroku.com/apps/"$1"/formation -d "$BODY" \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $2" \
  -H "Accept: application/vnd.heroku+json; version=3.docker-releases"
fi
