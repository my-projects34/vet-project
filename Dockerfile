FROM openjdk:11-jre-slim
VOLUME /tmp
ARG JAR_FILE
COPY ${JAR_FILE} vet.jar
CMD java -Djava.security.egd=file:/dev/./urandom -Dserver.port=$PORT -jar /vet.jar