package com.veterinary.demo.service;

import com.veterinary.demo.exception.*;
import com.veterinary.demo.model.Pet;
import com.veterinary.demo.model.TimeInterval;
import com.veterinary.demo.model.Visit;
import com.veterinary.demo.model.VisitStatus;
import com.veterinary.demo.model.dto.VisitRequestDTO;
import com.veterinary.demo.repository.VisitRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class VisitService {
    private final VisitRepository visitRepository;
    private final PetService petService;
    private static final Logger logger = LoggerFactory.getLogger(VisitService.class);

    public List<Visit> getVisits() {
        logger.info("Returning list of all visits to the client.");
        return visitRepository.findAll();
    }

    public List<Visit> getVisitsByDate(LocalDate date) {
        logger.info("Returning list of all visits selected by date to the client.");
        return visitRepository.findAllByDate(date);
    }

    public void deleteVisit(int visitId) {
        Visit visit = visitRepository.findById(visitId).orElseThrow(() -> {
            throw new NoSuchVisitException(visitId);
        });
        visitRepository.delete(visit);
        logger.info("Visit with ID: {} deleted.", visitId);
    }

    public Visit changeVisitStatus(int visitId, VisitStatus status) {
        Visit visit = visitRepository.findById(visitId).orElseThrow(() -> {
            throw new NoSuchVisitException(visitId);
        });
        visit.setStatus(status);
        logger.info("Status of visit with ID: {} changed to {}.", visitId, status);
        return visitRepository.save(visit);
    }

    public Visit addVisit(VisitRequestDTO visitRequestDTO) {
        Pet pet = petService.getPet(visitRequestDTO.getPetId()).orElseThrow(() -> {
            throw new NoSuchPetException(visitRequestDTO.getPetId());
        });
        if (pet.getClient() == null) {
            throw new WrongPetDataException("Pet with ID: " + visitRequestDTO.getPetId() + " has no client assigned.");
        }
        if (pet.getDeath() != null) {
            throw new WrongPetDataException("Pet with ID: " + visitRequestDTO.getPetId() + " is not alive.");
        }
        if (!visitRequestDTO.getDate().isAfter(LocalDate.now())) {
            throw new WrongVisitDataException("You can only book a visit for the next day/days.");
        }
        TimeInterval visitToBookInterval = new TimeInterval(visitRequestDTO.getTime(), visitRequestDTO.getTime().plusMinutes(visitRequestDTO.getDuration().getDuration()));
        if (visitToBookInterval.getFrom().isBefore(LocalTime.of(8, 0)) || visitToBookInterval.getTo().isAfter(LocalTime.of(20, 0))) {
            throw new WrongVisitDataException("You can only book a visit from 8:00 - 20:00.");
        }
        List<TimeInterval> alreadyBookedIntervals = visitRepository.findAllByDate(visitRequestDTO.getDate())
                .stream()
                .map(x -> new TimeInterval(x.getTime(), x.getTime().plusMinutes(x.getDuration().getDuration())))
                .sorted(Comparator.comparing(TimeInterval::getFrom))
                .collect(Collectors.toList());
        for (TimeInterval alreadyBookedInterval : alreadyBookedIntervals) {
            if ((alreadyBookedInterval.getFrom().isBefore(visitToBookInterval.getFrom()) && alreadyBookedInterval.getTo().isAfter(visitToBookInterval.getFrom()))
                    || (!(alreadyBookedInterval.getFrom().isBefore(visitToBookInterval.getFrom())) && alreadyBookedInterval.getFrom().isBefore(visitToBookInterval.getTo())))
                throw new VisitCollisionException(visitToBookInterval.getFrom(), visitToBookInterval.getTo());
        }
        logger.info("Visit for {} saved. Details: {} {}, {} minutes.", pet.getName(), visitRequestDTO.getDate(), visitRequestDTO.getTime(), visitRequestDTO.getDuration().getDuration());
        return visitRepository.save(Visit.builder()
                .date(visitRequestDTO.getDate())
                .duration(visitRequestDTO.getDuration())
                .pet(pet)
                .status(VisitStatus.BOOKED)
                .time(visitRequestDTO.getTime())
                .build());
    }
}
