package com.veterinary.demo.service;

import com.veterinary.demo.exception.NoSuchClientException;
import com.veterinary.demo.exception.NoSuchPetException;
import com.veterinary.demo.exception.WrongClientDataException;
import com.veterinary.demo.model.Client;
import com.veterinary.demo.model.Pet;
import com.veterinary.demo.model.dto.ClientRequestDTO;
import com.veterinary.demo.model.dto.PetResponseDTO;
import com.veterinary.demo.model.dto.VisitResponseDTO;
import com.veterinary.demo.repository.ClientRepository;
import com.veterinary.demo.utils.ClientUtils;
import com.veterinary.demo.utils.PetUtils;
import com.veterinary.demo.utils.VisitUtils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class ClientService {

    private static final Logger logger = LoggerFactory.getLogger(ClientService.class);
    private final ClientRepository clientRepository;
    private final PetService petService;

    public List<VisitResponseDTO> getClientVisits(int clientId) {
        Client client = clientRepository.findById(clientId).orElseThrow(() -> {
            throw new NoSuchClientException(clientId);
        });
        logger.info("Returning list of visits of client with ID: {}.", clientId);
        return client.getPets().stream()
                .map(Pet::getVisits)
                .flatMap(Collection::stream)
                .map(VisitUtils::convertVisitToDTO)
                .collect(Collectors.toList());
    }

    public Client addClient(ClientRequestDTO clientRequestDTO) {
        if (!ClientUtils.isAddressCorrect(clientRequestDTO.getAddress())) {
            throw new WrongClientDataException(clientRequestDTO.getAddress());
        }
        if (!ClientUtils.isContactCorrect(clientRequestDTO.getContact())) {
            throw new WrongClientDataException(clientRequestDTO.getContact());
        }
        logger.info("Client {} {} is being saved to the database.", clientRequestDTO.getName(), clientRequestDTO.getSurname());
        return clientRepository.save(ClientUtils.convertDTOtoClient(clientRequestDTO));
    }

    public List<Client> getClients() {
        logger.info("Returning list of clients.");
        return clientRepository.findAll();
    }

    public boolean assignPetToClient(int petId, int clientId) {
        Pet pet = petService.getPet(petId).orElseThrow(() -> {
            throw new NoSuchPetException(petId);
        });
        Client client = clientRepository.findById(clientId).orElseThrow(() -> {
            throw new NoSuchClientException(clientId);
        });

        if (client.assignPet(pet)) {
            pet.assignClient(client);
            clientRepository.save(client);
            logger.info("Pet with ID: {} is now assigned to client with ID: {}.", petId, clientId);
            return true;
        }
        logger.error("Assignment of Pet with ID: {} to the client with ID: {} failed: pet already assigned.", petId, clientId);
        return false;
    }

    public boolean deleteAssignment(int petId, int clientId) {
        Pet pet = petService.getPet(petId).orElseThrow(() -> {
            throw new NoSuchPetException(petId);
        });
        Client client = clientRepository.findById(clientId).orElseThrow(() -> {
            throw new NoSuchClientException(clientId);
        });

        if (client.deletePet(pet)) {
            if (pet.deleteClient(client)) {
                clientRepository.save(client);
                logger.info("Pet with ID: {} is now not assigned to client with ID: {}.", petId, clientId);
                return true;
            }
        }
        logger.error("Deleting assignment of Pet with ID: {} with the client with ID: {} failed.", petId, clientId);
        return false;
    }

    public void deleteClient(int clientId) {
        Client client = clientRepository.findById(clientId).orElseThrow(() -> {
            throw new NoSuchClientException(clientId);
        });
        clientRepository.delete(client);
        logger.info("Client with ID: {} deleted.", clientId);
    }

    public List<PetResponseDTO> getClientPets(int clientId) {
        Optional<Client> optionalClient = clientRepository.findById(clientId);
        if (!optionalClient.isPresent()) {
            throw new NoSuchClientException(clientId);
        }
        logger.info("Returning list of pets of client with ID: {}.", clientId);
        return optionalClient.get()
                .getPets()
                .stream()
                .map(PetUtils::convertPetToDTO)
                .collect(Collectors.toList());
    }
}
