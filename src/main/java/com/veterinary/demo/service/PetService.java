package com.veterinary.demo.service;

import com.veterinary.demo.exception.NoSuchPetException;
import com.veterinary.demo.exception.WrongPetDataException;
import com.veterinary.demo.model.Pet;
import com.veterinary.demo.model.dto.PetRequestDTO;
import com.veterinary.demo.model.dto.VisitResponseDTO;
import com.veterinary.demo.repository.PetRepository;
import com.veterinary.demo.utils.PetUtils;
import com.veterinary.demo.utils.VisitUtils;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PetService {

    private final PetRepository petRepository;
    private static final Logger logger = LoggerFactory.getLogger(PetService.class);

    public Optional<Pet> getPet(int petId) {
        return petRepository.findById(petId);
    }

    public List<Pet> getPets() {
        logger.info("Returning list of pets.");
        return petRepository.findAll();
    }

    public Pet addPet(PetRequestDTO petRequestDTO) {
        if (!PetUtils.isBirthDateCorrect(petRequestDTO.getBirth())) {
            throw new WrongPetDataException("Wrong date of birth provided.");
        }
        if (petRequestDTO.getDeath() != null && !PetUtils.isDeathDateCorrect(petRequestDTO.getDeath(), petRequestDTO.getBirth())) {
            throw new WrongPetDataException("Wrong date of death provided.");
        }
        logger.info("Pet {} is being saved to the database.", petRequestDTO.getName());
        return petRepository.save(PetUtils.convertDTOtoPet(petRequestDTO));
    }

    public void deletePet(int petId) {
        Pet pet = petRepository.findById(petId).orElseThrow(() -> {
            throw new NoSuchPetException(petId);
        });
        petRepository.delete(pet);
        logger.info("Pet with ID: {} deleted.", petId);
    }

    public List<VisitResponseDTO> getPetVisits(int petId) {
        Pet pet = petRepository.findById(petId).orElseThrow(() -> {
            throw new NoSuchPetException(petId);
        });
        logger.info("Returning list of visits of pet with ID: {}.", petId);
        return pet.getVisits().stream()
                .map(VisitUtils::convertVisitToDTO)
                .collect(Collectors.toList());
    }
}
