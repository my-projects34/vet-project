package com.veterinary.demo.exception;

import lombok.Getter;

import java.util.NoSuchElementException;

@Getter
public class NoSuchVisitException extends NoSuchElementException {
    private final int visitId;

    public NoSuchVisitException(int visitId) {
        super("Visit with ID: " + visitId + " not found.");
        this.visitId = visitId;
    }
}
