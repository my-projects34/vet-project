package com.veterinary.demo.exception;

public class WrongVisitDataException extends IllegalArgumentException {
    public WrongVisitDataException(String s) {
        super(s);
    }
}
