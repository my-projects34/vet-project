package com.veterinary.demo.exception.handler;

import com.veterinary.demo.exception.NoSuchVisitException;
import com.veterinary.demo.exception.VisitCollisionException;
import com.veterinary.demo.exception.WrongVisitDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class VisitExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(VisitExceptionHandler.class);

    @ExceptionHandler({WrongVisitDataException.class})
    public ResponseEntity<String> interceptWrongVisitDataException(WrongVisitDataException ex) {
        logger.error(ex.getMessage());
        return ResponseEntity.status(404).body(ex.getMessage());
    }

    @ExceptionHandler({VisitCollisionException.class})
    public ResponseEntity<String> interceptVisitCollisionException(VisitCollisionException ex) {
        logger.error("User failed to book a visit from {} to {}.", ex.getFrom(), ex.getTo());
        return ResponseEntity.status(409).body(ex.getMessage());
    }

    @ExceptionHandler({NoSuchVisitException.class})
    public ResponseEntity<String> interceptNoSuchVisitException(NoSuchVisitException ex) {
        logger.error("Attempt to find visit with ID: {} failed. Visit not present in the database.", ex.getVisitId());
        return ResponseEntity.status(404).body(ex.getMessage());
    }
}