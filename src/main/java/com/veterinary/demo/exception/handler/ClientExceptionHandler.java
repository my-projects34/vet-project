package com.veterinary.demo.exception.handler;

import com.veterinary.demo.exception.NoSuchClientException;
import com.veterinary.demo.exception.WrongClientDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ClientExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(ClientExceptionHandler.class);

    @ExceptionHandler({WrongClientDataException.class})
    public ResponseEntity<String> interceptWrongClientDataException(WrongClientDataException ex) {
        logger.error("Client provided wrong contact/address details. Provided: {}.", ex.getContact());
        return ResponseEntity.status(422).body(ex.getMessage());
    }

    @ExceptionHandler({NoSuchClientException.class})
    public ResponseEntity<String> interceptNoSuchClientException(NoSuchClientException ex) {
        logger.error("Attempt to find client with ID: {} failed. Client not present in the database.", ex.getClientId());
        return ResponseEntity.status(404).body(ex.getMessage());
    }
}
