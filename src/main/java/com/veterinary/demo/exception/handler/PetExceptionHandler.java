package com.veterinary.demo.exception.handler;

import com.veterinary.demo.exception.NoSuchPetException;
import com.veterinary.demo.exception.WrongPetDataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class PetExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(PetExceptionHandler.class);

    @ExceptionHandler({NoSuchPetException.class})
    public ResponseEntity<String> interceptNoSuchPetException(NoSuchPetException ex) {
        logger.error("Attempt to find pet with ID: {} failed. Pet not present in the database.", ex.getPetId());
        return ResponseEntity.status(404).body(ex.getMessage());
    }

    @ExceptionHandler({WrongPetDataException.class})
    public ResponseEntity<String> interceptWrongPetDataException(WrongPetDataException ex) {
        logger.error(ex.getMessage());
        return ResponseEntity.status(422).body(ex.getMessage());
    }
}
