package com.veterinary.demo.exception;

import lombok.Getter;

import java.util.NoSuchElementException;

@Getter
public class NoSuchClientException extends NoSuchElementException {
    private final int clientId;

    public NoSuchClientException(int clientId) {
        super("Client with ID: " + clientId + " not found.");
        this.clientId = clientId;
    }
}