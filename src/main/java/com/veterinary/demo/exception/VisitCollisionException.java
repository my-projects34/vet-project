package com.veterinary.demo.exception;

import lombok.Getter;

import java.time.LocalTime;

@Getter
public class VisitCollisionException extends IllegalArgumentException {
    private final LocalTime from;
    private final LocalTime to;

    public VisitCollisionException(LocalTime from, LocalTime to) {
        super("This time (" + from + " " + to + ") for a visit is not available.");
        this.from = from;
        this.to = to;
    }
}
