package com.veterinary.demo.exception;

import lombok.Getter;

@Getter
public class WrongClientDataException extends IllegalArgumentException {
    String contact;

    public WrongClientDataException(String contact) {
        super("Wrong contact/address details provided.");
        this.contact = contact;
    }
}