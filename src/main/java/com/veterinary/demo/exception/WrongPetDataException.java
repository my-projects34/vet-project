package com.veterinary.demo.exception;

public class WrongPetDataException extends IllegalArgumentException {
    public WrongPetDataException(String s) {
        super(s);
    }
}