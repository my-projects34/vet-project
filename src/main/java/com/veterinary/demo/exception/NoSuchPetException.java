package com.veterinary.demo.exception;

import lombok.Getter;

import java.util.NoSuchElementException;

@Getter
public class NoSuchPetException extends NoSuchElementException {
    private final int petId;

    public NoSuchPetException(int petId) {
        super("Pet with ID: " + petId + " not found.");
        this.petId = petId;
    }
}
