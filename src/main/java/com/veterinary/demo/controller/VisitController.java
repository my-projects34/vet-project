package com.veterinary.demo.controller;

import com.veterinary.demo.model.VisitStatus;
import com.veterinary.demo.model.dto.VisitRequestDTO;
import com.veterinary.demo.service.VisitService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@RestController
@AllArgsConstructor
@RequestMapping("/visits")
public class VisitController {
    private final VisitService visitService;

    @GetMapping
    public ResponseEntity<?> getAllVisits() {
        return ResponseEntity.ok(visitService.getVisits());
    }

    @PostMapping
    public ResponseEntity<?> addVisit(@RequestBody VisitRequestDTO visitRequestDTO) {
        return ResponseEntity.ok(visitService.addVisit(visitRequestDTO));
    }

    @GetMapping("/{date}")
    public ResponseEntity<?> getAllVisitsByDate(@PathVariable String date) {
        return ResponseEntity.ok(visitService.getVisitsByDate(LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE)));
    }

    @PatchMapping("/{visitId}/status/{status}")
    public ResponseEntity<?> changeVisitStatus(@PathVariable int visitId, @PathVariable VisitStatus status) {
        return ResponseEntity.ok(visitService.changeVisitStatus(visitId, status));
    }

    @DeleteMapping("/{visitId}")
    public ResponseEntity<?> deleteVisit(@PathVariable int visitId) {
        visitService.deleteVisit(visitId);
        return ResponseEntity.ok("Visit deleted successfully.");
    }
}
