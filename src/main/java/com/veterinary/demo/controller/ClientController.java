package com.veterinary.demo.controller;

import com.veterinary.demo.model.Client;
import com.veterinary.demo.model.dto.ClientRequestDTO;
import com.veterinary.demo.model.dto.PetResponseDTO;
import com.veterinary.demo.model.dto.VisitResponseDTO;
import com.veterinary.demo.service.ClientService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@AllArgsConstructor
@RequestMapping("/clients")
public class ClientController {

    private final ClientService clientService;

    @GetMapping
    public ResponseEntity<List<Client>> getAllClients() {
        return ResponseEntity.ok(clientService.getClients());
    }

    @GetMapping("/{clientId}/pets")
    public ResponseEntity<List<PetResponseDTO>> getAllClientPets(@PathVariable int clientId) {
        return ResponseEntity.ok(clientService.getClientPets(clientId));
    }

    @GetMapping("/{clientId}/visits")
    public ResponseEntity<List<VisitResponseDTO>> getAllClientVisits(@PathVariable int clientId) {
        return ResponseEntity.ok(clientService.getClientVisits(clientId));
    }

    @PostMapping
    public ResponseEntity<Client> addClient(@RequestBody ClientRequestDTO clientRequestDTO) {
        return ResponseEntity.ok(clientService.addClient(clientRequestDTO));
    }

    @PatchMapping
    public ResponseEntity<String> assignPetToClient(@RequestParam int petId, @RequestParam int clientId) {
        if (clientService.assignPetToClient(petId, clientId)) {
            return ResponseEntity.ok("Pet assigned successfully.");
        } else {
            return ResponseEntity.status(409).body("Pet is already assigned to this client.");
        }
    }

    @DeleteMapping
    public ResponseEntity<String> deleteAssignment(@RequestParam int petId, @RequestParam int clientId) {
        if (clientService.deleteAssignment(petId, clientId)) {
            return ResponseEntity.ok("Assignment deleted successfully.");
        } else {
            return ResponseEntity.status(409).body("No such assignment.");
        }
    }

    @DeleteMapping("/{clientId}")
    public ResponseEntity<String> deleteClient(@PathVariable int clientId) {
        clientService.deleteClient(clientId);
        return ResponseEntity.ok("Client with ID: " + clientId + " deleted successfully.");
    }

}
