package com.veterinary.demo.controller;

import com.veterinary.demo.model.dto.PetRequestDTO;
import com.veterinary.demo.service.PetService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/pets")
public class PetController {
    private final PetService petService;

    @GetMapping
    public ResponseEntity<?> getAllPets() {
        return ResponseEntity.ok(petService.getPets());
    }

    @GetMapping("/{petId}/visits")
    public ResponseEntity<?> getAllPetVisits(@PathVariable int petId) {
        return ResponseEntity.ok(petService.getPetVisits(petId));
    }

    @PostMapping
    public ResponseEntity<?> addPet(@RequestBody PetRequestDTO petRequestDTO) {
        return ResponseEntity.ok(petService.addPet(petRequestDTO));
    }

    @DeleteMapping("/{petId}")
    public ResponseEntity<?> deletePetClient(@PathVariable int petId) {
        petService.deletePet(petId);
        return ResponseEntity.ok("Pet with ID: " + petId + " deleted successfully.");
    }
}
