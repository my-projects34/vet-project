package com.veterinary.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Builder
@Table(name = "pet")
public class Pet {
    @JsonProperty("id")
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pet_id")
    private int id;

    @NotNull
    @Column(name = "species")
    @JsonProperty("species")
    @Enumerated(EnumType.STRING)
    private Species species;

    @NotNull
    @Column(name = "name")
    @JsonProperty("name")
    private String name;

    @NotNull
    @Column(name = "birth")
    @JsonProperty("birth")
    private String birth;

    @Column(name = "death")
    @JsonProperty("death")
    private LocalDate death;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "client_id")
    @JsonManagedReference
    private Client client;

    @OneToMany(mappedBy = "pet", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonBackReference
    private Set<Visit> visits;

    public boolean assignVisit(Visit visit) {
        return visits.add(visit);
    }

    public boolean deleteVisit(Visit visit) {
        return visits.remove(visit);
    }

    public boolean assignClient(Client client) {
        if (this.client != null)
            return false;
        else {
            this.client = client;
            return true;
        }
    }

    public boolean deleteClient(Client client) {
        if (this.client != null && this.client.equals(client)) {
            this.client = null;
            return true;
        } else {
            return false;
        }
    }
}
