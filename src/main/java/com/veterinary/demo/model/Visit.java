package com.veterinary.demo.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Builder
@Table(name = "visit")
public class Visit {
    @JsonProperty("id")
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "visit_id")
    private int id;

    @NotNull
    @Column(name = "duration")
    @JsonProperty("duration")
    @Enumerated(EnumType.STRING)
    private Duration duration;

    @NotNull
    @Column(name = "status")
    @JsonProperty("status")
    @Enumerated(EnumType.STRING)
    private VisitStatus status;

    @NotNull
    @Column(name = "date")
    @JsonProperty("date")
    private LocalDate date;

    @NotNull
    @Column(name = "start_time")
    @JsonProperty("time")
    private LocalTime time;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pet_id")
    @JsonManagedReference
    private Pet pet;
}
