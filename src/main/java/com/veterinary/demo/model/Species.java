package com.veterinary.demo.model;

public enum Species {
    DOG,
    CAT,
    HAMSTER,
    HORSE
}