package com.veterinary.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalTime;

@Data
@AllArgsConstructor
public class TimeInterval {
    LocalTime from;
    LocalTime to;
}
