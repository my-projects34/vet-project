package com.veterinary.demo.model;

public enum Duration {
    QUARTER(15),
    HALF_HOUR(30),
    THREE_QUARTERS(45),
    HOUR(60);

    private int minutes;

    Duration(int minutes) {
        this.minutes = minutes;
    }

    public int getDuration() {
        return this.minutes;
    }
}
