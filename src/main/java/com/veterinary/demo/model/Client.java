package com.veterinary.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Builder
@Table(name = "client")
public class Client {
    @JsonProperty("id")
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_id")
    private int id;

    @NotNull
    @Column(name = "name")
    @JsonProperty("name")
    private String name;

    @NotNull
    @Column(name = "surname")
    @JsonProperty("surname")
    private String surname;

    @NotNull
    @Column(name = "address")
    @JsonProperty("address")
    private String address;

    @NotNull
    @Column(name = "contact")
    @JsonProperty("contact")
    private String contact;

    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonBackReference
    private Set<Pet> pets;

    public boolean assignPet(Pet pet) {
        return pets.add(pet);
    }

    public boolean deletePet(Pet pet) {
        return pets.remove(pet);
    }
}
