package com.veterinary.demo.model;

public enum VisitStatus {
    BOOKED,
    COMPLETED,
    NOT_PRESENT
}
