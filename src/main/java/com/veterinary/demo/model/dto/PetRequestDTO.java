package com.veterinary.demo.model.dto;

import com.veterinary.demo.model.Species;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PetRequestDTO {
    private Species species;
    private String name;
    private String birth;
    private LocalDate death;
}
