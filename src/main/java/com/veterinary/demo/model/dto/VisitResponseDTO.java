package com.veterinary.demo.model.dto;

import com.veterinary.demo.model.Duration;
import com.veterinary.demo.model.VisitStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VisitResponseDTO {
    private int visitId;
    private Duration duration;
    private VisitStatus status;
    private String date;
    private int petId;
}
