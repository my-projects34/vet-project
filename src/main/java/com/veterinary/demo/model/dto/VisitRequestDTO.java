package com.veterinary.demo.model.dto;

import com.veterinary.demo.model.Duration;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VisitRequestDTO {
    private LocalDate date;
    private LocalTime time;
    private int petId;
    private Duration duration;
}
