package com.veterinary.demo.model.dto;

import com.veterinary.demo.model.Species;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class PetResponseDTO {
    private Species species;
    private String name;
    private String birth;
    private LocalDate death;
}
