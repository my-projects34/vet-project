package com.veterinary.demo.utils;

import com.veterinary.demo.model.Pet;
import com.veterinary.demo.model.dto.PetRequestDTO;
import com.veterinary.demo.model.dto.PetResponseDTO;

import java.time.LocalDate;
import java.util.Collections;

public class PetUtils {
    public static boolean isDeathDateCorrect(LocalDate death, String birth) {
        if (birth.contains("-")) {
            int year = Integer.parseInt(birth.split("-")[0]);
            int month = Integer.parseInt(birth.split("-")[1]);
            return !LocalDate.of(year, month, 1).isAfter(death);
        } else {
            int year = Integer.parseInt(birth);
            return !LocalDate.of(year, 1, 1).isAfter(death);
        }
    }

    public static boolean isBirthDateCorrect(String birth) {
        birth = birth.trim();
        if (!birth.matches("^(([1][9][0-9][0-9])|([2][0][0-9][0-9]))(([-](([0][1-9])|([1][0-2])))?)$"))
            return false;
        if (birth.contains("-")) {
            int year = Integer.parseInt(birth.split("-")[0]);
            int month = Integer.parseInt(birth.split("-")[1]);
            return !LocalDate.of(year, month, 1).isAfter(LocalDate.now());
        } else {
            int year = Integer.parseInt(birth);
            return !LocalDate.of(year, 1, 1).isAfter(LocalDate.now());
        }
    }

    // can also be a static method in Pet class
    public static Pet convertDTOtoPet(PetRequestDTO petRequestDTO) {
        return Pet.builder()
                .name(petRequestDTO.getName())
                .species(petRequestDTO.getSpecies())
                .birth(petRequestDTO.getBirth().trim())
                .death(petRequestDTO.getDeath())
                .client(null)
                .visits(Collections.emptySet())
                .build();
    }

    public static PetResponseDTO convertPetToDTO(Pet pet) {
        return PetResponseDTO.builder()
                .name(pet.getName())
                .species(pet.getSpecies())
                .birth(pet.getBirth())
                .death(pet.getDeath())
                .build();
    }
}
