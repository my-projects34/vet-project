package com.veterinary.demo.utils;

import com.veterinary.demo.model.Visit;
import com.veterinary.demo.model.dto.VisitResponseDTO;

public class VisitUtils {
    // can also be a static method in Visit class
    public static VisitResponseDTO convertVisitToDTO(Visit visit) {
        return VisitResponseDTO.builder()
                .date(visit.getDate() + " " + visit.getTime())
                .duration(visit.getDuration())
                .petId(visit.getPet().getId())
                .status(visit.getStatus())
                .visitId(visit.getId())
                .build();
    }
}
