package com.veterinary.demo.utils;

import com.veterinary.demo.model.Client;
import com.veterinary.demo.model.dto.ClientRequestDTO;

import java.util.Collections;

public class ClientUtils {

    public static boolean isContactCorrect(String contact) {
        contact = contact.trim();
        if (contact.split(" ").length == 2) {
            String[] contacts = contact.split(" ");
            return (contacts[0].matches("^(.+)@(.+)$") && contacts[1].matches("^[0-9]{9}$")) || (contacts[0].matches("^[0-9]{9}$") && contacts[1].matches("^(.+)@(.+)$"));
        } else if (contact.split(" ").length == 1) {
            return contact.matches("^(.+)@(.+)$") || contact.matches("^[0-9]{9}$");
        }
        return false;
    }

    public static boolean isAddressCorrect(String address) {
        return !address.trim().equals("");
    }

    // can also be a static method in Client class
    public static Client convertDTOtoClient(ClientRequestDTO clientRequestDTO) {
        return Client.builder()
                .name(clientRequestDTO.getName())
                .surname(clientRequestDTO.getSurname())
                .address(clientRequestDTO.getAddress().trim())
                .contact(clientRequestDTO.getContact().trim())
                .pets(Collections.emptySet())
                .build();
    }
}
