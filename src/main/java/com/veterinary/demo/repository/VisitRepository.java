package com.veterinary.demo.repository;

import com.veterinary.demo.model.Visit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface VisitRepository extends JpaRepository<Visit, Integer> {
    List<Visit> findAllByDate(LocalDate date);
}
