package com.veterinary.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.veterinary.demo.exception.NoSuchClientException;
import com.veterinary.demo.exception.NoSuchPetException;
import com.veterinary.demo.exception.WrongClientDataException;
import com.veterinary.demo.exception.handler.ClientExceptionHandler;
import com.veterinary.demo.exception.handler.PetExceptionHandler;
import com.veterinary.demo.model.Client;
import com.veterinary.demo.model.dto.ClientRequestDTO;
import com.veterinary.demo.model.dto.PetResponseDTO;
import com.veterinary.demo.model.dto.VisitResponseDTO;
import com.veterinary.demo.service.ClientService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@ExtendWith(MockitoExtension.class)
class ClientControllerTest {

    private MockMvc mockMvc;

    @Mock
    private ClientService clientService;

    @InjectMocks
    private ClientController clientController;

    @BeforeEach
    private void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(clientController)
                .setControllerAdvice(ClientExceptionHandler.class, PetExceptionHandler.class)
                .build();
    }

    @Test
    void getAllClientsShouldReturnListOfClientsWhenTheyExist() throws Exception {

        //given
        Client client1 = new Client();
        client1.setName("Jacek");
        Client client2 = new Client();
        client2.setName("Krzysiek");
        given(clientService.getClients()).willReturn(List.of(client1, client2));

        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/clients")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("Jacek", "Krzysiek");
    }

    @Test
    void getAllClientsShouldReturnEmptyListOfClientsWhenTheyDontExist() throws Exception {

        //given
        given(clientService.getClients()).willReturn(List.of());

        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/clients")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString().contains("[]"));
    }

    @Test
    void getAllClientPetsShouldReturnListOfPetsDTO() throws Exception {

        //given
        PetResponseDTO petResponseDTO1 = PetResponseDTO.builder().name("Burek").build();
        PetResponseDTO petResponseDTO2 = PetResponseDTO.builder().name("Lusia").build();
        given(clientService.getClientPets(1)).willReturn(List.of(petResponseDTO1, petResponseDTO2));

        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/clients/1/pets")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("Burek", "Lusia");
    }

    @Test
    void getAllClientPetsShouldReturnErrorWhenWrongClientId() throws Exception {

        //given
        given(clientService.getClientPets(-1)).willThrow(new NoSuchClientException(-1));

        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/clients/-1/pets")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
        assertThat(response.getContentAsString()).isEqualTo("Client with ID: -1 not found.");
    }

    @Test
    void getAllClientVisitsShouldReturnListOfVisitResponseDTO() throws Exception {

        //given
        VisitResponseDTO visitResponseDTO1 = VisitResponseDTO.builder().visitId(1).build();
        VisitResponseDTO visitResponseDTO2 = VisitResponseDTO.builder().visitId(2).build();
        given(clientService.getClientVisits(1)).willReturn(List.of(visitResponseDTO1, visitResponseDTO2));

        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/clients/1/visits")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("1", "2");
    }

    @Test
    void addClientShouldReturnClient() throws Exception {

        //given
        ClientRequestDTO clientRequestDTO = ClientRequestDTO.builder()
                .name("Jacek")
                .address("Cicha 21")
                .contact("111222333")
                .build();
        Client resultClient = new Client();
        resultClient.setName("Jacek");
        given(clientService.addClient(clientRequestDTO)).willReturn(resultClient);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                post("/clients")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(clientRequestDTO)))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("Jacek");
    }

    @Test
    void addClientShouldThrowExceptionWhenWrongAddress() throws Exception {

        //given
        ClientRequestDTO clientRequestDTO = ClientRequestDTO.builder()
                .name("Jacek")
                .address("")
                .contact("111222333")
                .build();
        Client resultClient = new Client();
        resultClient.setName("Jacek");
        given(clientService.addClient(clientRequestDTO)).willThrow(WrongClientDataException.class);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                post("/clients")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(clientRequestDTO)))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());
    }

    @Test
    void addClientShouldThrowExceptionWhenWrongContact() throws Exception {

        //given
        ClientRequestDTO clientRequestDTO = ClientRequestDTO.builder()
                .name("Jacek")
                .address("Cicha 21")
                .contact("111222333444")
                .build();
        Client resultClient = new Client();
        resultClient.setName("Jacek");
        given(clientService.addClient(clientRequestDTO)).willThrow(WrongClientDataException.class);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                post("/clients")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(clientRequestDTO)))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());
    }

    @Test
    void assignPetToClientShouldReturnTrueWhenClientAndPetExist() throws Exception {

        //given
        String petId = "1";
        String clientId = "1";
        given(clientService.assignPetToClient(1, 1)).willReturn(true);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.patch("/clients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("petId", petId)
                        .param("clientId", clientId))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo("Pet assigned successfully.");
    }

    @Test
    void assignPetToClientShouldThrowExceptionWhenWrongClientId() throws Exception {

        //given
        String petId = "1";
        String clientId = "-1";
        given(clientService.assignPetToClient(1, -1)).willThrow(NoSuchClientException.class);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.patch("/clients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("petId", petId)
                        .param("clientId", clientId))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    void assignPetToClientShouldThrowExceptionWhenWrongPetId() throws Exception {

        //given
        String petId = "-1";
        String clientId = "1";
        given(clientService.assignPetToClient(-1, 1)).willThrow(NoSuchPetException.class);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.patch("/clients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("petId", petId)
                        .param("clientId", clientId))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    void assignPetToClientShouldReturnFalseWhenPetAlreadyAssigned() throws Exception {

        //given
        String petId = "1";
        String clientId = "1";
        given(clientService.assignPetToClient(1, 1)).willReturn(false);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.patch("/clients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("petId", petId)
                        .param("clientId", clientId))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.CONFLICT.value());
        assertThat(response.getContentAsString()).isEqualTo("Pet is already assigned to this client.");
    }

    @Test
    void deleteAssignmentShouldReturnTrueWhenClientAndPetExist() throws Exception {

        //given
        String petId = "1";
        String clientId = "1";
        given(clientService.deleteAssignment(1, 1)).willReturn(true);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.delete("/clients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("petId", petId)
                        .param("clientId", clientId))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo("Assignment deleted successfully.");
    }

    @Test
    void deleteAssignmentShouldThrowExceptionWhenWrongClientId() throws Exception {

        //given
        String petId = "1";
        String clientId = "-1";
        given(clientService.deleteAssignment(1, -1)).willThrow(NoSuchClientException.class);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.delete("/clients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("petId", petId)
                        .param("clientId", clientId))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    void deleteAssignmentShouldThrowExceptionWhenWrongPetId() throws Exception {

        //given
        String petId = "-1";
        String clientId = "1";
        given(clientService.deleteAssignment(-1, 1)).willThrow(NoSuchPetException.class);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.delete("/clients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("petId", petId)
                        .param("clientId", clientId))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    void deleteAssignmentShouldReturnFalseWhenNoSuchAssignment() throws Exception {

        //given
        String petId = "1";
        String clientId = "1";
        given(clientService.deleteAssignment(1, 1)).willReturn(false);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                MockMvcRequestBuilders.delete("/clients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("petId", petId)
                        .param("clientId", clientId))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.CONFLICT.value());
        assertThat(response.getContentAsString()).isEqualTo("No such assignment.");
    }
}