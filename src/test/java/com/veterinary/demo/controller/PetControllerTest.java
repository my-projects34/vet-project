package com.veterinary.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.veterinary.demo.exception.NoSuchPetException;
import com.veterinary.demo.exception.WrongPetDataException;
import com.veterinary.demo.exception.handler.PetExceptionHandler;
import com.veterinary.demo.model.Pet;
import com.veterinary.demo.model.Species;
import com.veterinary.demo.model.dto.PetRequestDTO;
import com.veterinary.demo.model.dto.VisitResponseDTO;
import com.veterinary.demo.service.PetService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@ExtendWith(MockitoExtension.class)
class PetControllerTest {

    private MockMvc mockMvc;

    @Mock
    private PetService petService;

    @InjectMocks
    private PetController petController;

    @BeforeEach
    private void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(petController)
                .setControllerAdvice(PetExceptionHandler.class)
                .build();
    }


    @Test
    void getAllPetsShouldReturnListOfPetsWhenTheyExist() throws Exception {

        //given
        Pet pet1 = new Pet();
        pet1.setName("Burek");
        Pet pet2 = new Pet();
        pet2.setName("Lusia");
        given(petService.getPets()).willReturn(List.of(pet1, pet2));

        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/pets")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("Burek", "Lusia");
    }

    @Test
    void getAllPetsShouldReturnEmptyListWhenPetsDontExist() throws Exception {

        //given
        given(petService.getPets()).willReturn(List.of());

        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/pets")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("[]");
    }

    @Test
    void getAllPetVisitsShouldReturnListOfVisitsDTO() throws Exception {

        //given
        int petId = 1;
        VisitResponseDTO visitResponseDTO1 = VisitResponseDTO.builder().visitId(1).build();
        VisitResponseDTO visitResponseDTO2 = VisitResponseDTO.builder().visitId(2).build();
        given(petService.getPetVisits(petId)).willReturn(List.of(visitResponseDTO1, visitResponseDTO2));

        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/pets/1/visits")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("1", "2");
    }

    @Test
    void getAllPetVisitsShouldThrowExceptionWhenNoSuchPet() throws Exception {

        //given
        int petId = 1;
        given(petService.getPetVisits(petId)).willThrow(NoSuchPetException.class);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/pets/1/visits")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    void addPetShouldReturnPet() throws Exception {

        //given
        PetRequestDTO petRequestDTO = PetRequestDTO.builder()
                .name("Burek")
                .birth("2019")
                .death(null)
                .species(Species.DOG)
                .build();
        Pet resultPet = new Pet();
        resultPet.setName("Burek");
        given(petService.addPet(petRequestDTO)).willReturn(resultPet);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                post("/pets")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(petRequestDTO)))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("Burek");
    }

    @Test
    void addPetShouldThrowExceptionWhenWrongBirthDate() throws Exception {

        //given
        PetRequestDTO petRequestDTO = PetRequestDTO.builder()
                .name("Burek")
                .birth(String.valueOf(LocalDate.now().getYear() + 1))
                .death(null)
                .species(Species.DOG)
                .build();
        given(petService.addPet(petRequestDTO)).willThrow(WrongPetDataException.class);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                post("/pets")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(petRequestDTO)))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());
    }

    @Test
    void addPetShouldThrowExceptionWhenWrongDeathDate() throws Exception {

        //given
        PetRequestDTO petRequestDTO = PetRequestDTO.builder()
                .name("Burek")
                .birth(String.valueOf(LocalDate.now().getYear() - 1))
                .death(null)
                .species(Species.DOG)
                .build();
        given(petService.addPet(petRequestDTO)).willThrow(WrongPetDataException.class);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                post("/pets")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(petRequestDTO)))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY.value());
    }
}