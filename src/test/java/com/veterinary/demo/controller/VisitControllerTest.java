package com.veterinary.demo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.veterinary.demo.exception.NoSuchVisitException;
import com.veterinary.demo.exception.VisitCollisionException;
import com.veterinary.demo.exception.WrongVisitDataException;
import com.veterinary.demo.exception.handler.PetExceptionHandler;
import com.veterinary.demo.exception.handler.VisitExceptionHandler;
import com.veterinary.demo.model.Visit;
import com.veterinary.demo.model.VisitStatus;
import com.veterinary.demo.model.dto.VisitRequestDTO;
import com.veterinary.demo.service.VisitService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@ExtendWith(MockitoExtension.class)
class VisitControllerTest {

    private MockMvc mockMvc;

    @Mock
    private VisitService visitService;

    @InjectMocks
    private VisitController visitController;

    @BeforeEach
    private void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(visitController)
                .setControllerAdvice(VisitExceptionHandler.class, PetExceptionHandler.class)
                .build();
    }

    @Test
    void getAllVisitsShouldReturnListOfVisits() throws Exception {

        //given
        Visit visit1 = new Visit();
        visit1.setId(1);
        Visit visit2 = new Visit();
        visit2.setId(2);
        given(visitService.getVisits()).willReturn(List.of(visit1, visit2));

        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/visits")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("1", "2");
    }

    @Test
    void getAllVisitsShouldReturnEmptyListOfVisitsWhenNoVisits() throws Exception {

        //given
        given(visitService.getVisits()).willReturn(List.of());

        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/visits")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("[]");
    }

    @Test
    void addVisitShouldReturnVisit() throws Exception {

        //given
        VisitRequestDTO visitRequestDTO = VisitRequestDTO.builder()
                .petId(1)
                .build();
        Visit returnVisit = new Visit();
        returnVisit.setId(2);
        given(visitService.addVisit(visitRequestDTO)).willReturn(returnVisit);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                post("/visits")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(visitRequestDTO)))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("2");
    }

    @Test
    void addVisitShouldThrowExceptionWhenColliding() throws Exception {

        //given
        VisitRequestDTO visitRequestDTO = VisitRequestDTO.builder()
                .petId(1)
                .build();
        given(visitService.addVisit(visitRequestDTO)).willThrow(VisitCollisionException.class);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                post("/visits")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(visitRequestDTO)))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.CONFLICT.value());
    }

    @Test
    void addVisitShouldThrowExceptionWhenWrongVisitData() throws Exception {

        //given
        VisitRequestDTO visitRequestDTO = VisitRequestDTO.builder()
                .petId(1)
                .build();
        given(visitService.addVisit(visitRequestDTO)).willThrow(WrongVisitDataException.class);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                post("/visits")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(visitRequestDTO)))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    void getAllVisitsByDateShouldReturnVisits() throws Exception {

        //given
        Visit visit1 = new Visit();
        visit1.setId(1);
        Visit visit2 = new Visit();
        visit2.setId(2);
        given(visitService.getVisitsByDate(any())).willReturn(List.of(visit1, visit2));

        //when
        MockHttpServletResponse response = mockMvc.perform(
                get("/visits/2020-05-29")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("1", "2");
    }

    @Test
    void changeVisitStatusShouldChangeStatus() throws Exception {

        //given
        Visit visit = new Visit();
        visit.setId(1);
        visit.setStatus(VisitStatus.BOOKED);
        Visit resultVisit = new Visit();
        resultVisit.setId(1);
        resultVisit.setStatus(VisitStatus.COMPLETED);
        given(visitService.changeVisitStatus(1, VisitStatus.COMPLETED)).willReturn(resultVisit);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                patch("/visits/1/status/COMPLETED")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).contains("COMPLETED");
    }

    @Test
    void changeVisitStatusShouldThrowExceptionWhenNoSuchVisit() throws Exception {

        //given
        given(visitService.changeVisitStatus(1, VisitStatus.COMPLETED)).willThrow(NoSuchVisitException.class);

        //when
        MockHttpServletResponse response = mockMvc.perform(
                patch("/visits/1/status/COMPLETED")
                        .accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        //then
        assertThat(response.getStatus()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }
}