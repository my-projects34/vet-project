package com.veterinary.demo.service;

import com.veterinary.demo.exception.*;
import com.veterinary.demo.model.*;
import com.veterinary.demo.model.dto.VisitRequestDTO;
import com.veterinary.demo.repository.VisitRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class VisitServiceTest {
    @Mock
    private PetService petService;
    @Mock
    private VisitRepository visitRepository;

    @Test
    void getVisitsShouldReturnListOfVisits() {
        //given
        VisitService visitService = new VisitService(visitRepository, petService);
        Pet pet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(null)
                .client(null)
                .visits(Collections.emptySet())
                .id(1)
                .build();
        Visit visit = Visit.builder()
                .id(1)
                .pet(pet)
                .date(LocalDate.now().plusDays(2))
                .duration(Duration.HALF_HOUR)
                .status(VisitStatus.BOOKED)
                .time(LocalTime.of(9, 30))
                .build();
        Set<Visit> visitSet = new HashSet<>();
        visitSet.add(visit);
        pet.setVisits(visitSet);
        given(visitRepository.findAll()).willReturn(List.of(visit));

        //when
        List<Visit> resultList = visitService.getVisits();

        //then
        assertThat(resultList).containsExactly(visit);
    }

    @Test
    void getVisitsByDateShouldReturnListOfVisits() {
        //given
        VisitService visitService = new VisitService(visitRepository, petService);
        Pet pet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(null)
                .client(null)
                .visits(Collections.emptySet())
                .id(1)
                .build();
        Visit visit = Visit.builder()
                .id(1)
                .pet(pet)
                .date(LocalDate.now().plusDays(2))
                .duration(Duration.HALF_HOUR)
                .status(VisitStatus.BOOKED)
                .time(LocalTime.of(9, 30))
                .build();
        Set<Visit> visitSet = new HashSet<>();
        visitSet.add(visit);
        pet.setVisits(visitSet);
        given(visitRepository.findAllByDate(LocalDate.now().plusDays(2))).willReturn(List.of(visit));

        //when
        List<Visit> resultList = visitService.getVisitsByDate(LocalDate.now().plusDays(2));

        //then
        assertThat(resultList).containsExactly(visit);
    }

    @Test
    void deleteVisitShouldThrowExceptionWhenVisitNotFound() {
        //given
        VisitService visitService = new VisitService(visitRepository, petService);
        int visitId = -1;
        given(visitRepository.findById(visitId)).willReturn(Optional.empty());

        //then
        assertThrows(NoSuchVisitException.class, () -> visitService.deleteVisit(-1));
    }

    @Test
    void deleteVisitShouldNotThrowExceptionWhenVisitDeleted() {
        //given
        VisitService visitService = new VisitService(visitRepository, petService);
        int visitId = 1;
        Visit visit = new Visit();
        given(visitRepository.findById(visitId)).willReturn(Optional.of(visit));

        //then
        assertDoesNotThrow(() -> visitService.deleteVisit(visitId));
    }

    @Test
    void changeVisitStatusWillChangeStatusWhenVisitFound() {
        //given
        VisitService visitService = new VisitService(visitRepository, petService);
        int visitId = 1;
        Pet pet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(null)
                .client(null)
                .visits(Collections.emptySet())
                .id(1)
                .build();
        Visit visit = Visit.builder()
                .id(1)
                .pet(pet)
                .date(LocalDate.now().plusDays(2))
                .duration(Duration.HALF_HOUR)
                .status(VisitStatus.BOOKED)
                .time(LocalTime.of(9, 30))
                .build();
        Set<Visit> visitSet = new HashSet<>();
        visitSet.add(visit);
        pet.setVisits(visitSet);

        Pet updatedPet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(null)
                .client(null)
                .visits(Collections.emptySet())
                .id(1)
                .build();
        Visit updatedVisit = Visit.builder()
                .id(1)
                .pet(pet)
                .date(LocalDate.now().plusDays(2))
                .duration(Duration.HALF_HOUR)
                .status(VisitStatus.COMPLETED)
                .time(LocalTime.of(9, 30))
                .build();
        Set<Visit> visitSet2 = new HashSet<>();
        visitSet2.add(updatedVisit);
        updatedPet.setVisits(visitSet2);

        given(visitRepository.findById(visitId)).willReturn(Optional.of(visit));
        given(visitRepository.save(any())).willReturn(updatedVisit);
        //when
        Visit result = visitService.changeVisitStatus(visitId, VisitStatus.COMPLETED);

        //then
        assertThat(result).isEqualTo(updatedVisit);
    }

    @Test
    void changeVisitStatusShouldThrowExceptionWhenVisitNotFound() {
        //given
        VisitService visitService = new VisitService(visitRepository, petService);
        int visitId = -1;
        VisitStatus visitStatus = VisitStatus.BOOKED;
        given(visitRepository.findById(visitId)).willReturn(Optional.empty());

        //then
        assertThrows(NoSuchVisitException.class, () -> visitService.changeVisitStatus(visitId, visitStatus));
    }

    @Test
    void addVisitShouldThrowExceptionWhenVisitNotFound() {
        //given
        VisitService visitService = new VisitService(visitRepository, petService);
        VisitRequestDTO visitRequestDTO = VisitRequestDTO.builder()
                .date(LocalDate.now().plusDays(1))
                .petId(1)
                .duration(Duration.HALF_HOUR)
                .time(LocalTime.of(9, 0))
                .build();
        given(petService.getPet(1)).willReturn(Optional.empty());

        //then
        assertThrows(NoSuchPetException.class, () -> visitService.addVisit(visitRequestDTO));
    }

    @Test
    void addVisitShouldThrowExceptionWhenPetHasNoClient() {
        //given
        VisitService visitService = new VisitService(visitRepository, petService);
        VisitRequestDTO visitRequestDTO = VisitRequestDTO.builder()
                .date(LocalDate.now().plusDays(1))
                .petId(1)
                .duration(Duration.HALF_HOUR)
                .time(LocalTime.of(9, 0))
                .build();
        Pet pet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(null)
                .client(null)
                .visits(Collections.emptySet())
                .id(1)
                .build();
        given(petService.getPet(1)).willReturn(Optional.of(pet));

        //then
        assertThrows(WrongPetDataException.class, () -> visitService.addVisit(visitRequestDTO));
    }

    @Test
    void addVisitShouldThrowExceptionWhenPetIsNotAlive() {
        //given
        VisitService visitService = new VisitService(visitRepository, petService);
        VisitRequestDTO visitRequestDTO = VisitRequestDTO.builder()
                .date(LocalDate.now().plusDays(1))
                .petId(1)
                .duration(Duration.HALF_HOUR)
                .time(LocalTime.of(9, 0))
                .build();
        Pet pet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(LocalDate.of(2020, 6, 14))
                .client(new Client())
                .visits(Collections.emptySet())
                .id(1)
                .build();
        given(petService.getPet(1)).willReturn(Optional.of(pet));

        //then
        assertThrows(WrongPetDataException.class, () -> visitService.addVisit(visitRequestDTO));
    }

    @Test
    void addVisitShouldThrowExceptionWhenVisitBookedForTheSameDayOrPrevious() {
        //given
        VisitService visitService = new VisitService(visitRepository, petService);
        VisitRequestDTO visitRequestDTO = VisitRequestDTO.builder()
                .date(LocalDate.now())
                .petId(1)
                .duration(Duration.HALF_HOUR)
                .time(LocalTime.of(9, 0))
                .build();
        Pet pet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(null)
                .client(new Client())
                .visits(Collections.emptySet())
                .id(1)
                .build();
        given(petService.getPet(1)).willReturn(Optional.of(pet));

        //then
        assertThrows(WrongVisitDataException.class, () -> visitService.addVisit(visitRequestDTO));
    }

    @Test
    void addVisitShouldThrowExceptionWhenVisitBookedBefore8AM() {
        //given
        VisitService visitService = new VisitService(visitRepository, petService);
        VisitRequestDTO visitRequestDTO = VisitRequestDTO.builder()
                .date(LocalDate.now().plusDays(1))
                .petId(1)
                .duration(Duration.HALF_HOUR)
                .time(LocalTime.of(7, 59))
                .build();
        Pet pet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(null)
                .client(new Client())
                .visits(Collections.emptySet())
                .id(1)
                .build();
        given(petService.getPet(1)).willReturn(Optional.of(pet));

        //then
        assertThrows(WrongVisitDataException.class, () -> visitService.addVisit(visitRequestDTO));
    }

    @Test
    void addVisitShouldThrowExceptionWhenVisitEndsAfter8PM() {
        //given
        VisitService visitService = new VisitService(visitRepository, petService);
        VisitRequestDTO visitRequestDTO = VisitRequestDTO.builder()
                .date(LocalDate.now().plusDays(1))
                .petId(1)
                .duration(Duration.HALF_HOUR)
                .time(LocalTime.of(19, 31))
                .build();
        Pet pet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(null)
                .client(new Client())
                .visits(Collections.emptySet())
                .id(1)
                .build();
        given(petService.getPet(1)).willReturn(Optional.of(pet));

        //then
        assertThrows(WrongVisitDataException.class, () -> visitService.addVisit(visitRequestDTO));
    }

    @Test
    void addVisitShouldThrowExceptionWhenVisitCollidingWithOthers() {
        Visit visit1 = Visit.builder()
                .date(LocalDate.now().plusDays(1))
                .duration(Duration.HALF_HOUR)
                .time(LocalTime.of(8, 45))
                .build();
        Visit visit2 = Visit.builder()
                .date(LocalDate.now().plusDays(1))
                .duration(Duration.HALF_HOUR)
                .time(LocalTime.of(9, 15))
                .build();
        Visit visit3 = Visit.builder()
                .date(LocalDate.now().plusDays(1))
                .duration(Duration.HOUR)
                .time(LocalTime.of(8, 45))
                .build();
        Visit visit4 = Visit.builder()
                .date(LocalDate.now().plusDays(1))
                .duration(Duration.HALF_HOUR)
                .time(LocalTime.of(9, 0))
                .build();
        Visit visit5 = Visit.builder()
                .date(LocalDate.now().plusDays(1))
                .duration(Duration.QUARTER)
                .time(LocalTime.of(9, 10))
                .build();
        parametrizedAddVisitShouldThrowExceptionWhenVisitCollidingWithOthers(visit1);
        parametrizedAddVisitShouldThrowExceptionWhenVisitCollidingWithOthers(visit2);
        parametrizedAddVisitShouldThrowExceptionWhenVisitCollidingWithOthers(visit3);
        parametrizedAddVisitShouldThrowExceptionWhenVisitCollidingWithOthers(visit4);
        parametrizedAddVisitShouldThrowExceptionWhenVisitCollidingWithOthers(visit5);
    }

    void parametrizedAddVisitShouldThrowExceptionWhenVisitCollidingWithOthers(Visit visit) {
        //given
        VisitService visitService = new VisitService(visitRepository, petService);
        VisitRequestDTO visitRequestDTO = VisitRequestDTO.builder()
                .date(LocalDate.now().plusDays(1))
                .petId(1)
                .duration(Duration.HALF_HOUR)
                .time(LocalTime.of(9, 0))
                .build();
        Pet pet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(null)
                .client(new Client())
                .visits(Collections.emptySet())
                .id(1)
                .build();

        given(petService.getPet(1)).willReturn(Optional.of(pet));
        given(visitRepository.findAllByDate(LocalDate.now().plusDays(1))).willReturn(List.of(visit));

        //then
        assertThrows(VisitCollisionException.class, () -> visitService.addVisit(visitRequestDTO));
    }

    @Test
    void addVisitShouldReturnVisit() {
        Visit visit1 = Visit.builder()
                .date(LocalDate.now().plusDays(1))
                .duration(Duration.HALF_HOUR)
                .time(LocalTime.of(8, 30))
                .build();
        Visit visit2 = Visit.builder()
                .date(LocalDate.now().plusDays(1))
                .duration(Duration.HALF_HOUR)
                .time(LocalTime.of(9, 30))
                .build();
        parametrizedAddVisitShouldReturnVisit(List.of(visit1));
        parametrizedAddVisitShouldReturnVisit(List.of(visit2));
        parametrizedAddVisitShouldReturnVisit(List.of(visit1, visit2));
    }

    void parametrizedAddVisitShouldReturnVisit(List<Visit> visitList) {
        //given
        VisitService visitService = new VisitService(visitRepository, petService);
        VisitRequestDTO visitRequestDTO = VisitRequestDTO.builder()
                .date(LocalDate.now().plusDays(1))
                .petId(1)
                .duration(Duration.HALF_HOUR)
                .time(LocalTime.of(9, 0))
                .build();
        Pet pet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(null)
                .client(new Client())
                .visits(Collections.emptySet())
                .id(1)
                .build();
        Visit builtVisit = Visit.builder()
                .date(visitRequestDTO.getDate())
                .duration(visitRequestDTO.getDuration())
                .pet(pet)
                .status(VisitStatus.BOOKED)
                .time(visitRequestDTO.getTime())
                .build();
        given(petService.getPet(1)).willReturn(Optional.of(pet));
        given(visitRepository.findAllByDate(LocalDate.now().plusDays(1))).willReturn(visitList);
        given(visitRepository.save(any())).willReturn(builtVisit);

        //when
        Visit result = visitService.addVisit(visitRequestDTO);

        //then
        assertThat(result).isEqualTo(builtVisit);
    }


}