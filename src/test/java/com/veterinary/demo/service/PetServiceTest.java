package com.veterinary.demo.service;

import com.veterinary.demo.exception.NoSuchPetException;
import com.veterinary.demo.exception.WrongPetDataException;
import com.veterinary.demo.model.*;
import com.veterinary.demo.model.dto.PetRequestDTO;
import com.veterinary.demo.model.dto.VisitResponseDTO;
import com.veterinary.demo.repository.PetRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class PetServiceTest {

    @Mock
    private PetRepository petRepository;

    @Test
    void getPetShouldReturnPetWhenItsInDatabase() {
        //given
        int petId = 1;
        PetService petService = new PetService(petRepository);
        Pet pet = Pet.builder()
                .name("Burek")
                .birth("2020-01")
                .death(null)
                .species(Species.DOG)
                .visits(Collections.emptySet())
                .client(null)
                .id(1)
                .build();
        given(petRepository.findById(petId)).willReturn(Optional.ofNullable(pet));

        //when
        Optional<Pet> optionalPet = petService.getPet(petId);

        //then
        assertTrue(optionalPet.isPresent());
        assertThat(optionalPet.get().getName()).isEqualTo("Burek");
    }

    @Test
    void getPetShouldReturnEmptyOptionalWhenItsNotInDatabase() {
        //given
        int petId = 1;
        PetService petService = new PetService(petRepository);
        given(petRepository.findById(petId)).willReturn(Optional.empty());

        //when
        Optional<Pet> optionalPet = petService.getPet(petId);

        //then
        assertFalse(optionalPet.isPresent());
    }

    @Test
    void getPetsShouldReturnListOfPetsWhenDatabaseNotEmpty() {
        //given
        PetService petService = new PetService(petRepository);
        Pet pet = Pet.builder()
                .name("Burek")
                .birth("2020-01")
                .death(null)
                .species(Species.DOG)
                .visits(Collections.emptySet())
                .client(null)
                .id(1)
                .build();
        List<Pet> list = new ArrayList<>();
        list.add(pet);
        given(petRepository.findAll()).willReturn(list);

        //when
        List<Pet> resultList = petService.getPets();

        //then
        assertThat(resultList).containsExactly(pet);
    }

    @Test
    void addPetShouldThrowExceptionWhenWrongBirthDateProvided() {
        //given
        PetService petService = new PetService(petRepository);
        PetRequestDTO petRequestDTO1 = PetRequestDTO.builder()
                .name("Burek")
                .birth(Integer.valueOf(LocalDate.now().getYear() + 1).toString())
                .death(null)
                .species(Species.DOG)
                .build();
        PetRequestDTO petRequestDTO2 = PetRequestDTO.builder()
                .name("Burek")
                .birth(LocalDate.now().getYear() + "-" + (LocalDate.now().getMonthValue() + 1 <= 9 ? "0" + (LocalDate.now().getMonthValue() + 1) : LocalDate.now().getMonthValue() + 1))
                .death(null)
                .species(Species.DOG)
                .build();
        PetRequestDTO petRequestDTO3 = PetRequestDTO.builder()
                .name("Burek")
                .birth("05-2020")
                .death(null)
                .species(Species.DOG)
                .build();
        PetRequestDTO petRequestDTO4 = PetRequestDTO.builder()
                .name("Burek")
                .birth("2020-05-14")
                .death(null)
                .species(Species.DOG)
                .build();

        //then
        assertThrows(WrongPetDataException.class, () -> petService.addPet(petRequestDTO1));
        assertThrows(WrongPetDataException.class, () -> petService.addPet(petRequestDTO2));
        assertThrows(WrongPetDataException.class, () -> petService.addPet(petRequestDTO3));
        assertThrows(WrongPetDataException.class, () -> petService.addPet(petRequestDTO4));
    }

    @Test
    void addPetShouldReturnPetWhenGoodBirthDateWithYearProvided() {
        //given
        PetService petService = new PetService(petRepository);
        PetRequestDTO petRequestDTO = PetRequestDTO.builder()
                .name("Burek")
                .birth(Integer.valueOf(LocalDate.now().getYear()).toString())
                .death(null)
                .species(Species.DOG)
                .build();
        Pet pet = Pet.builder()
                .name(petRequestDTO.getName())
                .species(petRequestDTO.getSpecies())
                .birth(petRequestDTO.getBirth().trim())
                .death(petRequestDTO.getDeath())
                .client(null)
                .visits(Collections.emptySet())
                .id(1)
                .build();
        given(petRepository.save(any())).willReturn(pet);

        //when
        Pet resultPet = petService.addPet(petRequestDTO);

        //then
        assertThat(resultPet.getId()).isEqualTo(1);
    }

    @Test
    void addPetShouldReturnPetWhenGoodBirthDateWithYearAndMonthProvided() {
        //given
        PetService petService = new PetService(petRepository);
        PetRequestDTO petRequestDTO = PetRequestDTO.builder()
                .name("Burek")
                .birth(LocalDate.now().getYear() + "-" + (LocalDate.now().getMonthValue() <= 9 ? "0" + LocalDate.now().getMonthValue() : LocalDate.now().getMonthValue()))
                .death(null)
                .species(Species.DOG)
                .build();
        Pet pet = Pet.builder()
                .name(petRequestDTO.getName())
                .species(petRequestDTO.getSpecies())
                .birth(petRequestDTO.getBirth().trim())
                .death(petRequestDTO.getDeath())
                .client(null)
                .visits(Collections.emptySet())
                .id(1)
                .build();
        given(petRepository.save(any())).willReturn(pet);

        //when
        Pet resultPet = petService.addPet(petRequestDTO);

        //then
        assertThat(resultPet.getId()).isEqualTo(1);
    }

    @Test
    void addPetShouldThrowExceptionWhenWrongDeathDateProvided() {
        //given
        PetService petService = new PetService(petRepository);
        PetRequestDTO petRequestDTO1 = PetRequestDTO.builder()
                .name("Burek")
                .birth("2020-01")
                .death(LocalDate.of(2019, 12, 30))
                .species(Species.DOG)
                .build();
        PetRequestDTO petRequestDTO2 = PetRequestDTO.builder()
                .name("Burek")
                .birth("2020")
                .death(LocalDate.of(2019, 12, 31))
                .species(Species.DOG)
                .build();

        //then
        assertThrows(WrongPetDataException.class, () -> petService.addPet(petRequestDTO1));
        assertThrows(WrongPetDataException.class, () -> petService.addPet(petRequestDTO2));
    }

    @Test
    void addPetShouldReturnPetWhenGoodDeathDateProvidedWhenBirthDateYearAndMonth() {
        //given
        PetService petService = new PetService(petRepository);
        PetRequestDTO petRequestDTO = PetRequestDTO.builder()
                .name("Burek")
                .birth(LocalDate.now().getYear() + "-" + (LocalDate.now().getMonthValue() <= 9 ? "0" + LocalDate.now().getMonthValue() : LocalDate.now().getMonthValue()))
                .death(LocalDate.now())
                .species(Species.DOG)
                .build();
        Pet pet = Pet.builder()
                .name(petRequestDTO.getName())
                .species(petRequestDTO.getSpecies())
                .birth(petRequestDTO.getBirth().trim())
                .death(petRequestDTO.getDeath())
                .client(null)
                .visits(Collections.emptySet())
                .id(1)
                .build();
        given(petRepository.save(any())).willReturn(pet);

        //when
        Pet resultPet = petService.addPet(petRequestDTO);

        //then
        assertThat(resultPet.getId()).isEqualTo(1);
    }

    @Test
    void addPetShouldReturnPetWhenGoodDeathDateProvidedWhenBirthDateYearOnly() {
        //given
        PetService petService = new PetService(petRepository);
        PetRequestDTO petRequestDTO = PetRequestDTO.builder()
                .name("Burek")
                .birth(Integer.valueOf(LocalDate.now().getYear()).toString())
                .death(LocalDate.now())
                .species(Species.DOG)
                .build();
        Pet pet = Pet.builder()
                .name(petRequestDTO.getName())
                .species(petRequestDTO.getSpecies())
                .birth(petRequestDTO.getBirth().trim())
                .death(petRequestDTO.getDeath())
                .client(null)
                .visits(Collections.emptySet())
                .id(1)
                .build();
        given(petRepository.save(any())).willReturn(pet);

        //when
        Pet resultPet = petService.addPet(petRequestDTO);

        //then
        assertThat(resultPet.getId()).isEqualTo(1);
    }

    @Test
    void deletePetShouldThrowExceptionWhenWrongPetId() {
        //given
        PetService petService = new PetService(petRepository);
        int petId = 1;
        given(petRepository.findById(petId)).willReturn(Optional.empty());

        //then
        assertThrows(NoSuchPetException.class, () -> petService.deletePet(petId));
    }

    @Test
    void deletePetShouldNotThrowExceptionPetExists() {
        //given
        PetService petService = new PetService(petRepository);
        int petId = 1;
        Pet pet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(null)
                .client(null)
                .visits(Collections.emptySet())
                .id(1)
                .build();
        given(petRepository.findById(petId)).willReturn(Optional.of(pet));

        //then
        assertDoesNotThrow(() -> petService.deletePet(1));
    }

    @Test
    void getPetVisitsShouldThrowExceptionWhenWrongPetId() {
        //given
        PetService petService = new PetService(petRepository);
        int petId = 1;
        given(petRepository.findById(petId)).willReturn(Optional.empty());

        //then
        assertThrows(NoSuchPetException.class, () -> petService.getPetVisits(petId));
    }

    @Test
    void getPetVisitsShouldReturnNonEmptyListWhenPetWithVisits() {
        //given
        PetService petService = new PetService(petRepository);
        int petId = 1;

        Pet pet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(null)
                .client(null)
                .visits(Collections.emptySet())
                .id(1)
                .build();
        Visit visit = Visit.builder()
                .id(1)
                .pet(pet)
                .date(LocalDate.now().plusDays(2))
                .duration(Duration.HALF_HOUR)
                .status(VisitStatus.BOOKED)
                .time(LocalTime.of(9, 30))
                .build();
        Set<Visit> visitSet = new HashSet<>();
        visitSet.add(visit);
        pet.setVisits(visitSet);
        given(petRepository.findById(petId)).willReturn(Optional.of(pet));

        //when
        List<VisitResponseDTO> result = petService.getPetVisits(petId);

        //then
        assertFalse(result.isEmpty());
    }

    @Test
    void getPetVisitsShouldReturnEmptyListWhenPetWithNoVisits() {
        //given
        PetService petService = new PetService(petRepository);
        int petId = 1;

        Pet pet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(null)
                .client(null)
                .visits(Collections.emptySet())
                .id(1)
                .build();
        Set<Visit> visitSet = new HashSet<>();
        pet.setVisits(visitSet);
        given(petRepository.findById(petId)).willReturn(Optional.of(pet));

        //when
        List<VisitResponseDTO> result = petService.getPetVisits(petId);

        //then
        assertTrue(result.isEmpty());
    }
}