package com.veterinary.demo.service;

import com.veterinary.demo.exception.NoSuchClientException;
import com.veterinary.demo.exception.NoSuchPetException;
import com.veterinary.demo.exception.WrongClientDataException;
import com.veterinary.demo.model.*;
import com.veterinary.demo.model.dto.ClientRequestDTO;
import com.veterinary.demo.model.dto.PetResponseDTO;
import com.veterinary.demo.model.dto.VisitResponseDTO;
import com.veterinary.demo.repository.ClientRepository;
import com.veterinary.demo.utils.PetUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class ClientServiceTest {

    @Mock
    private ClientRepository clientRepository;

    @Mock
    private PetService petService;

    @Test
    void getClientVisitsShouldThrowExceptionWhenClientNotFound() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        int clientId = -1;
        given(clientRepository.findById(clientId)).willReturn(Optional.empty());

        //then
        assertThrows(NoSuchClientException.class, () -> clientService.getClientVisits(clientId));
    }

    @Test
    void getClientVisitsShouldReturnListOfVisitsDTO() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        int clientId = 1;
        Client client = new Client();
        Pet pet = new Pet();
        pet.setId(1);
        client.setPets(Set.of(pet));
        Visit visit = Visit.builder()
                .id(1)
                .pet(pet)
                .time(LocalTime.of(9, 0))
                .date(LocalDate.now().plusDays(1))
                .duration(Duration.HALF_HOUR)
                .status(VisitStatus.BOOKED)
                .build();
        pet.setVisits(Set.of(visit));
        given(clientRepository.findById(clientId)).willReturn(Optional.of(client));
        VisitResponseDTO responseDTO = VisitResponseDTO.builder()
                .date(LocalDate.now().plusDays(1) + " " + LocalTime.of(9, 0))
                .duration(Duration.HALF_HOUR)
                .petId(1)
                .status(VisitStatus.BOOKED)
                .visitId(1)
                .build();

        //when
        List<VisitResponseDTO> dtoList = clientService.getClientVisits(clientId);

        //then
        assertThat(dtoList).containsExactly(responseDTO);
    }

    @Test
    void addClientShouldThrowExceptionWhenAddressNotCorrect() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        ClientRequestDTO clientRequestDTO = ClientRequestDTO.builder()
                .address(" ")
                .contact("111111111")
                .name("Jacek")
                .surname("Nowak")
                .build();

        //then
        assertThrows(WrongClientDataException.class, () -> clientService.addClient(clientRequestDTO));
    }

    @Test
    void addClientShouldThrowExceptionWhenContactNotCorrect() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        ClientRequestDTO clientRequestDTO = ClientRequestDTO.builder()
                .address("Sadowa 5G")
                .contact("1234567890")
                .name("Jacek")
                .surname("Nowak")
                .build();
        ClientRequestDTO clientRequestDTO2 = ClientRequestDTO.builder()
                .address("Sadowa 5G")
                .contact("aaaa.gmail.com")
                .name("Jacek")
                .surname("Nowak")
                .build();
        ClientRequestDTO clientRequestDTO3 = ClientRequestDTO.builder()
                .address("Sadowa 5G")
                .contact("aaaa@gmail.com 1234567890")
                .name("Jacek")
                .surname("Nowak")
                .build();
        ClientRequestDTO clientRequestDTO4 = ClientRequestDTO.builder()
                .address("Sadowa 5G")
                .contact("1234567890 aaaa@gmail.com")
                .name("Jacek")
                .surname("Nowak")
                .build();
        ClientRequestDTO clientRequestDTO5 = ClientRequestDTO.builder()
                .address("Sadowa 5G")
                .contact("1234567890 aaaa@gmail.com 2")
                .name("Jacek")
                .surname("Nowak")
                .build();
        ClientRequestDTO clientRequestDTO6 = ClientRequestDTO.builder()
                .address("Sadowa 5G")
                .contact(" ")
                .name("Jacek")
                .surname("Nowak")
                .build();


        //then
        assertThrows(WrongClientDataException.class, () -> clientService.addClient(clientRequestDTO));
        assertThrows(WrongClientDataException.class, () -> clientService.addClient(clientRequestDTO2));
        assertThrows(WrongClientDataException.class, () -> clientService.addClient(clientRequestDTO3));
        assertThrows(WrongClientDataException.class, () -> clientService.addClient(clientRequestDTO4));
        assertThrows(WrongClientDataException.class, () -> clientService.addClient(clientRequestDTO5));
        assertThrows(WrongClientDataException.class, () -> clientService.addClient(clientRequestDTO6));
    }

    @Test
    void addClientShouldReturnClientWhenSaved() {
        ClientRequestDTO clientRequestDTO = ClientRequestDTO.builder()
                .address("Sadowa 5G")
                .contact("123456789")
                .name("Jacek")
                .surname("Nowak")
                .build();
        ClientRequestDTO clientRequestDTO2 = ClientRequestDTO.builder()
                .address("Sadowa 5G")
                .contact(" a@gmail.com  ")
                .name("Jacek")
                .surname("Nowak")
                .build();
        ClientRequestDTO clientRequestDTO3 = ClientRequestDTO.builder()
                .address("Sadowa 5G")
                .contact(" 123456789 a@gmail.com ")
                .name("Jacek")
                .surname("Nowak")
                .build();
        ClientRequestDTO clientRequestDTO4 = ClientRequestDTO.builder()
                .address("Sadowa 5G")
                .contact(" a@gmail.com 123456789 ")
                .name("Jacek")
                .surname("Nowak")
                .build();

        parametrizedAddClientShouldReturnClientWhenSaved(clientRequestDTO);
        parametrizedAddClientShouldReturnClientWhenSaved(clientRequestDTO2);
        parametrizedAddClientShouldReturnClientWhenSaved(clientRequestDTO3);
        parametrizedAddClientShouldReturnClientWhenSaved(clientRequestDTO4);
    }

    void parametrizedAddClientShouldReturnClientWhenSaved(ClientRequestDTO clientRequestDTO) {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        Client convertedClient = Client.builder()
                .name(clientRequestDTO.getName())
                .surname(clientRequestDTO.getSurname())
                .address(clientRequestDTO.getAddress().trim())
                .contact(clientRequestDTO.getContact().trim())
                .pets(Collections.emptySet())
                .build();
        given(clientRepository.save(any())).willReturn(convertedClient);

        //when
        Client result = clientService.addClient(clientRequestDTO);

        //then
        assertThat(result).isEqualTo(convertedClient);
    }

    @Test
    void getClients() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        Client client = new Client();
        client.setId(1);
        given(clientRepository.findAll()).willReturn(List.of(client));

        //when
        List<Client> clients = clientService.getClients();

        //then
        assertThat(clients).containsExactly(client);
    }

    @Test
    void assignPetToClientShouldThrowExceptionWhenPetNotFound() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        int clientId = 1;
        int petId = -1;
        given(petService.getPet(petId)).willReturn(Optional.empty());

        //then
        assertThrows(NoSuchPetException.class, () -> clientService.assignPetToClient(petId, clientId));

    }

    @Test
    void assignPetToClientShouldThrowExceptionWhenClientNotFound() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        int clientId = -1;
        int petId = 1;
        given(petService.getPet(petId)).willReturn(Optional.of(new Pet()));
        given(clientRepository.findById(clientId)).willReturn(Optional.empty());
        //then
        assertThrows(NoSuchClientException.class, () -> clientService.assignPetToClient(petId, clientId));

    }

    @Test
    void assignPetToClientShouldReturnTrue() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        int clientId = 1;
        int petId = 1;
        Pet pet = new Pet();
        Client client = new Client();
        client.setPets(new HashSet<>());
        given(petService.getPet(petId)).willReturn(Optional.of(pet));
        given(clientRepository.findById(clientId)).willReturn(Optional.of(client));

        //when
        boolean result = clientService.assignPetToClient(petId, clientId);

        //then
        assertTrue(result);
    }

    @Test
    void assignPetToClientShouldReturnFalseWhenPetIsAlreadyAssigned() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        int clientId = 1;
        int petId = 1;
        Pet pet = new Pet();
        Client client = new Client();
        Set<Pet> pets = new HashSet<>();
        pets.add(pet);
        client.setPets(pets);
        given(petService.getPet(petId)).willReturn(Optional.of(pet));
        given(clientRepository.findById(clientId)).willReturn(Optional.of(client));

        //when
        boolean result = clientService.assignPetToClient(petId, clientId);

        //then
        assertFalse(result);
    }

    @Test
    void deleteAssignmentShouldThrowExceptionWhenPetNotFound() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        int clientId = 1;
        int petId = -1;
        given(petService.getPet(petId)).willReturn(Optional.empty());

        //then
        assertThrows(NoSuchPetException.class, () -> clientService.deleteAssignment(petId, clientId));
    }

    @Test
    void deleteAssignmentShouldThrowExceptionWhenClientNotFound() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        int clientId = -1;
        int petId = 1;
        given(petService.getPet(petId)).willReturn(Optional.of(new Pet()));
        given(clientRepository.findById(clientId)).willReturn(Optional.empty());
        //then
        assertThrows(NoSuchClientException.class, () -> clientService.deleteAssignment(petId, clientId));
    }

    @Test
    void deleteAssignmentShouldReturnFalseWhenPetNotAssigned() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        int clientId = 1;
        int petId = 1;
        Pet pet = new Pet();
        Client client = new Client();
        Set<Pet> pets = new HashSet<>();
        client.setPets(pets);
        given(petService.getPet(petId)).willReturn(Optional.of(pet));
        given(clientRepository.findById(clientId)).willReturn(Optional.of(client));

        //when
        boolean result = clientService.deleteAssignment(petId, clientId);

        //then
        assertFalse(result);
    }

    @Test
    void deleteAssignmentShouldReturnFalseWhenClientNotAssigned() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        int clientId = 1;
        int petId = 1;
        Pet pet = new Pet();
        Client client = new Client();
        Set<Pet> pets = new HashSet<>();
        pets.add(pet);
        client.setPets(pets);
        given(petService.getPet(petId)).willReturn(Optional.of(pet));
        given(clientRepository.findById(clientId)).willReturn(Optional.of(client));

        //when
        boolean result = clientService.deleteAssignment(petId, clientId);

        //then
        assertFalse(result);
    }

    @Test
    void deleteAssignmentShouldReturnTrue() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        int clientId = 1;
        int petId = 1;
        Pet pet = new Pet();
        Client client = new Client();
        Set<Pet> pets = new HashSet<>();
        pets.add(pet);
        client.setPets(pets);
        pet.setClient(client);
        given(petService.getPet(petId)).willReturn(Optional.of(pet));
        given(clientRepository.findById(clientId)).willReturn(Optional.of(client));

        //when
        boolean result = clientService.deleteAssignment(petId, clientId);

        //then
        assertTrue(result);
    }

    @Test
    void deleteClientShouldNotThrowExceptionWhenClientFound() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        Client client = new Client();
        int clientId = 1;
        given(clientRepository.findById(clientId)).willReturn(Optional.of(client));

        //then
        assertDoesNotThrow(() -> clientService.deleteClient(clientId));
    }

    @Test
    void deleteClientShouldThrowExceptionWhenClientNotFound() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        int clientId = -1;
        given(clientRepository.findById(clientId)).willReturn(Optional.empty());
        //then
        assertThrows(NoSuchClientException.class, () -> clientService.deleteClient(clientId));
    }

    @Test
    void getClientPetsShouldThrowExceptionWhenClientNotFound() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        int clientId = -1;
        given(clientRepository.findById(clientId)).willReturn(Optional.empty());
        //then
        assertThrows(NoSuchClientException.class, () -> clientService.getClientPets(clientId));
    }

    @Test
    void getClientPetsShouldReturnListOfPetsDTO() {
        //given
        ClientService clientService = new ClientService(clientRepository, petService);
        int clientId = 1;
        Client client = new Client();
        client.setPets(new HashSet<>());
        client.setId(clientId);
        Pet pet = Pet.builder()
                .name("Burek")
                .species(Species.DOG)
                .birth("2020-01")
                .death(null)
                .build();
        client.assignPet(pet);
        pet.setClient(client);

        given(clientRepository.findById(clientId)).willReturn(Optional.of(client));
        //when
        List<PetResponseDTO> result = clientService.getClientPets(clientId);

        //then
        assertThat(result).containsExactly(PetUtils.convertPetToDTO(pet));
    }
}