# Veterinary project

### Overview of the project
The project was made during classes at Jagiellonian University (subject: "Java in production applications").
It was to mainly learn about Spring Boot, Hibernate, logging, CI/CD, Heroku, testing (ex. JUnit).
The application bases on REST API, is covered by tests (API tests using karate framework will be added soon) and uses HEROKU POSTGRES.

### What is it about?
The idea of application is to provide clients to book visits for their pets.
- Clients can book a visit for their pets from 8 AM. to 8 PM. (last visit can't end after 8 PM.).
- Client can assign Pet to himself and remove the assignment (ex. client can't book a visit to a non-assigned pet).
- Visits can't collide with themselves (ex. 1st visit is from 10:00 to 10:30 and the 2nd is from 10:15 to 10:30).

There are some certain conditions that must be followed, here are some of them:
- contact is a simple regex for email and 9-digit phone number,
- birthday of a pet needs to look like:
    - year-month, ex. 1990-03
    - year, ex. 1991
- some fields can't be nullable,
- you can't book a visit for a dead pet.

There are also some basic functions for 
- GET calls, ex. return list of all clients, return list of visits for a particular pet, return clients' pets etc.
- POST calls, ex. add a client, add a pet, add a visit.
- DELETE calls, ex. delete client, delete pet, delete visit.

### How to run the application

To run the application you can simply run "./gradlew bootRun" command in the root directory of the project. The application will be available locally on port 8080.
You can also work on Heroku-deployed application.
URL: https://vet-brozek.herokuapp.com/
The application (both deployed and local) uses HEROKU POSTGRE database. It's already configured.

### Endpoints

/clients

GET - returns list of all Clients.

POST - add a Client.

    body:
    {
        "name": ... (type: String)
        "surname": ... (type: String)
        "address": ... (type: String)
        "contact": ... (type: String; email/9 digit phone number/both divided by space)
    }

/clients/{clientId}

DELETE - delete a Client with clientId.
    
/clients?petId=...&clientId=...

PATCH - assign Pet to a Client.

DELETE - delete assignment.

/clients/{clientId}/pets

GET - returns list of Pets for a specific Client with clientId.

/clients/{clientId}/visits

GET - returns list of all visits for a specific Client with clientId.

---------------------

/pets

GET - returns list of all Pets.

POST - add a Pet.

    body:
    
    {
        "species": ... (type: Species (enum: [DOG, CAT, HAMSTER, HORSE]))
        "name": ... (type: String)
        "birth": ... (type: String, format: "yyyy-mm" or "yyyy")
        "death": ... (type: LocalDate, ex. 2020-04-17, can be null)
    }

/pets/{petId}

DELETE - delete a Pet with specific petId.

/pets/{petId}/visits

GET - returns all Pet visits.

---------------------

/visits

GET - returns a list of all Visits.

POST - book a visit.

    body:
    {
        "date": ... (type: LocalDate)
        "time": ... (type: LocalTime)
        "petId": ... (type: int)
        "duration": ... (type: Duration (enum: [QUARTER, HALF_HOUR, THREE_QUARTERS, HOUR]))
    }

/visits/{visitId}

DELETE - delete Visit with a specific visitId.

/visits/{date}

GET - return list of Visits for a specific date.

/visits/{visitId}/status/{status}

PATCH - change status of a Visit.

### Additional information

There's a gitlab-ci.yml file which contains 3 stages:
- build
- test
- deploy (Heroku)

TODO soon: 
- Karate API tests.
- Swagger API documentation with all endpoints fully listed.